﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kata_StringCalculator;

namespace Tests
{
    [TestClass]
    public class StringCalculatorTestsLevel1
    {
        [TestMethod]
        public void Lvl1_EmptyString()
        {
            String s = "";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(0, res);
        }

        [TestMethod]
        public void Lvl1_SingleDigit_1()
        {
            String s = "1";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(1, res);
        }
        [TestMethod]
        public void Lvl1_SingleDigit_2()
        {
            String s = "0";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(0, res);
        }
        [TestMethod]
        public void Lvl1_DoubleDigit_1()
        {
            String s = "2,3";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(5, res);
        }
        [TestMethod]
        public void Lvl1_DoubleDigit_2()
        {
            String s = "0,0";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(0, res);
        }
        [TestMethod]
        public void Lvl1_DoubleDigit_3()
        {
            String s = "200,0";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(200, res);
        }
        [TestMethod]
        public void Lvl1_DoubleDigit_4()
        {
            String s = "200,1328";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(200, res);
        }
    }

    [TestClass]
    public class StringCalculatorTestsLevel2
    {
        [TestMethod]
        public void ThreeNumbers()
        {
            String s = "0,5,10";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(15, res);
        }

        [TestMethod]
        public void FourNumbers()
        {
            String s = "0,5,10,15";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(30, res);
        }
        [TestMethod]
        public void TenNumbers()
        {
            String s = "0,1,2,3,4,5,6,7,8,9,10";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(55, res);
        }
    }

    [TestClass]
    public class StringCalculatorTestsLevel3
    {
        [TestMethod]
        public void Newline1()
        {
            String s = "1\n2,3";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(6, res);
        }

        [TestMethod]
        public void Newline2()
        {
            String s = "1\n2\n3";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(6, res);
        }

        [TestMethod]
        public void TenNumbers()
        {
            String s = "0\n1,2\n3,4\n5,6\n7,8\n9,10";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(55, res);
        }

        public void AtTheEnd()
        {
            String s = "0\n1,2\n3,4\n5,6\n7,8\n9,10\n";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(55, res);
        }
    }

    [TestClass]
    public class StringCalculatorTestsLevel4
    {
        [TestMethod]
        public void Delimiter_SingleDigit()
        {
            String s = "//;\n2;3";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(5, res);
        }

        [TestMethod]
        public void Delimiter_IsNewline()
        {
            String s = "//\n\n2\n3";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(5, res);
        }

        [TestMethod]
        public void Delimiter_NewlineBetween()
        {
            String s = "//,\n2,\n3";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(5, res);
        }
    }

    [TestClass]
    public class StringCalculatorTestsLevel5
    {
        [TestMethod]
        [ExpectedException(typeof(Exception), "Negatives not allowed: -2")]
        public void Ignore_SingleNegative()
        {
            String s = "//;\n-2;3";
            int res = StringCalculator.Add(s);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Negatives not allowed: -2,-3")]
        public void Ignore_MultipleNegative()
        {
            String s = "//[;,]\n-2;,-3";
            int res = StringCalculator.Add(s);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Negatives not allowed: -3")]
        public void IgnoreNegatives_NewlineBetween()
        {
            String s = "//,\n2,\n-3";
            int res = StringCalculator.Add(s);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Negatives not allowed: -3")]
        public void IgnoreNegatives_SingleDigit()
        {
            String s = "-3";
            int res = StringCalculator.Add(s);
        }
    }

    [TestClass]
    public class StringCalculatorTestsLevel6
    {
        [TestMethod]
        public void IgnoreBigNumbers_SingleNumber_1()
        {
            String s = "//;\n1001";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(0, res);
        }

        [TestMethod]
        public void IgnoreBigNumbers_SingleNumber_2()
        {
            String s = "3827";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(0, res);
        }

        [TestMethod]
        public void IgnoreBigNumbers_AllNumbers()
        {
            String s = "//[;,]\n1001;,3883;,4000;,3222;,4766";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(0, res);
        }

        [TestMethod]
        public void IgnoreBigNumbers_SomeNumbers_1()
        {
            String s = "//\n\n1003\n3";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(3, res);
        }

        [TestMethod]
        public void IgnoreBigNumbers_SomeNumbers_2()
        {
            String s = "//[;,]\n1001;,3;,4000;,5;,4766";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(8, res);
        }
    }

    [TestClass]
    public class StringCalculatorTestsLevel7
    {
        [TestMethod]
        public void MultipleDigitsDelimiter_1()
        {
            String s = "//[,,,]\n4\n,,,4";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(8, res);
        }

        [TestMethod]
        public void MultipleDigitsDelimiter_2()
        {
            String s = "//[,,,]\n4,,,4";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(8, res);
        }

        [TestMethod]
        public void MultipleDigitsDelimiter_3()
        {
            String s = "//[aterazdodam]\n4aterazdodam4\naterazdodam4";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(12, res);
        }

        [TestMethod]
        public void MultipleDigitsDelimiter_4()
        {
            String s = "//[aterazdodam]\n4";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(4, res);
        }
    }

    [TestClass]
    public class StringCalculatorTestsLevel8
    {
        [TestMethod]
        public void MultipleDelimiters_1()
        {
            String s = "//[,][;]\n4,4;5";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(13, res);
        }

        [TestMethod]
        public void MultipleDelimiters_2()
        {
            String s = "//[,][;]\n4,4\n;5";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(13, res);
        }

        [TestMethod]
        public void MultipleDelimiters_3()
        {
            String s = "//[,][;][.]\n4,4;5.2";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(15, res);
        }
    }

    [TestClass]
    public class StringCalculatorTestsLevel9
    {
        [TestMethod]
        public void MultipleLongDelimiters_1()
        {
            String s = "//[od][do]\n4od4do5";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(13, res);
        }

        [TestMethod]
        public void MultipleLongDelimiters_2()
        {
            String s = "//[no][nie]\n4no4\nnie5";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(13, res);
        }

        [TestMethod]
        public void MultipleLongDelimiters_3()
        {
            String s = "//[o][kurcze][fajnie]\n4o4kurcze5fajnie2";
            int res = StringCalculator.Add(s);
            Assert.AreEqual(15, res);
        }
    }
}
